'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var HLSPlayer = (function () {
	function HLSPlayer(videoId) {
		_classCallCheck(this, HLSPlayer);

		this._videoElement = document.getElementById(videoId);
		this.mimeType = 'video/mp4;codecs=avc1.42c00d,mp4a.40.2';
		this.segments = ['media_w249832652_10713.ts', 'media_w249832652_10714.ts', 'media_w249832652_10715.ts'];
		this._currentSegment = 0;
		this._setupVideo();
	}

	_createClass(HLSPlayer, [{
		key: '_setupVideo',
		value: function _setupVideo() {
			var _this = this;

			if (!('MediaSource' in window)) {
				throw new Error('MediaSource not supported!');
			}
			this._mediaSource = new window.MediaSource();
			var url = URL.createObjectURL(this._mediaSource);
			this._videoElement.pause();
			this._videoElement.src = url;

			this._mediaSource.addEventListener('sourceopen', function () {
				_this._videoSource = _this._mediaSource.addSourceBuffer(_this.mimeType);
				_this._videoSource.addEventListener('updateend', function () {
					if (_this._videoElement.paused) {
						console.log('play');
						_this._videoElement.play();
					}
				});
				_this._initVideo();
			}, false);
		}
	}, {
		key: '_initVideo',
		value: function _initVideo() {
			this.__onSourceUpdate = this._onSourceUpdate.bind(this);
			this._playSegment(this.segments[this._currentSegment]);
			this._videoSource.addEventListener('update', this.__onSourceUpdate, false);
		}
	}, {
		key: '_onSourceUpdate',
		value: function _onSourceUpdate() {
			this._currentSegment++;
			this._playSegment(this.segments[this._currentSegment]);
			if (this._currentSegment >= this.segments.length - 1) {
				this._videoSource.removeEventListener('update', this.__onSourceUpdate, false);
			}
		}
	}, {
		key: '_playSegment',
		value: function _playSegment(fileName) {
			var _this2 = this;

			var xhr = new XMLHttpRequest();
			xhr.open('GET', fileName);
			xhr.send();
			xhr.responseType = 'arraybuffer';
			xhr.addEventListener('readystatechange', function (e) {
				if (xhr.readyState !== xhr.DONE) {
					return;
				}
				var reader = new FileReader();
				reader.onload = function (e) {
					_this2._videoSource.appendBuffer(new Uint8Array(e.target.result));
				};
				reader.readAsArrayBuffer(new Blob([new Uint8Array(xhr.response)], { type: 'video/mp2t' }));
				/*try {
    	this._videoSource.appendBuffer(new Uint8Array(xhr.response));
    } catch(exception){
    	console.log(fileName, exception.message, exception.stack);
    }*/
			}, false);
		}
	}]);

	return HLSPlayer;
})();

//# sourceMappingURL=hls-compiled.js.map