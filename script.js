class LivePlayer {
	constructor(url, videoId) {
		this._index = 0;
		this._lastTime = 0;
		this._bufferUpdated = false;
		this._videoSource = null;
		this._storeBaseUrl(url);
		this._getData(url);
		this._videoElement = document.getElementById(videoId);
	}

	_storeBaseUrl(url) {
		this._baseUrl = url.split('/');
		this._baseUrl.pop();
		this._baseUrl = this._baseUrl.join('/') + '/';
	}

	_getData(url) {
		if (!url) {
			return;
		}
		var xhr = new XMLHttpRequest();
		xhr.open('GET', url, true);
		xhr.responseType = 'text';
		xhr.send();

		xhr.onreadystatechange = () => {
			if (xhr.readyState === xhr.DONE) {
				var parser = new DOMParser();
				this._xmlData = parser.parseFromString(xhr.response, 'text/xml', 0);
				this._parseData();
			}
		};
	}

	_parseData() {
		this._adaptationSet = this._xmlData.querySelectorAll('AdaptationSet[mimeType*="video/"]').item(0);
		this._representations = this._adaptationSet.children;
		this._setupVideo();
	}

	_setupVideo() {
		if (!('MediaSource' in window)) {
			throw new Error('MediaSource not supported!');
		}
		this._mediaSource = new MediaSource();
		var url = URL.createObjectURL(this._mediaSource);
		this._videoElement.pause();
		this._videoElement.src = url;

		this._mediaSource.addEventListener('sourceopen', () => {
			this._videoSource = this._mediaSource.addSourceBuffer(this.mimeType);
			this._initVideo();
		}, false);
	}


	_initVideo() {
		var xhr = new XMLHttpRequest();
		xhr.open('GET', this._baseUrl + this.initializationSegment);
		xhr.send();
		xhr.responseType = 'arraybuffer';
		xhr.addEventListener('readystatechange', () => {
			if (xhr.readyState !== xhr.DONE) {
				return;
			}
			this._videoSource.appendBuffer(new Uint8Array(xhr.response));
			this.__onSourceUpdate = this._onSourceUpdate.bind(this);
			this._videoSource.addEventListener('update', this.__onSourceUpdate, false);
		}, false);
	}

	_onSourceUpdate() {
		this._bufferUpdated = true;
		this._getStarted();
		this._videoSource.removeEventListener('update', this.__onSourceUpdate, false);
		delete this.__onSourceUpdate;
	}

	_getStarted() {
		this.__fileChecks = this._fileChecks.bind(this);
		this._videoElement.addEventListener("timeupdate", this.__fileChecks, false);
		this._index = this.startNumber;
		this._playSegment();
		this._index++;
	}

	_playSegment() {
		var xhr = new XMLHttpRequest();
		xhr.open('GET', this._baseUrl + this.currentSegment);
		xhr.send();
		xhr.responseType = 'arraybuffer';
		xhr.addEventListener('readystatechange', () => {
			if (xhr.readyState !== xhr.DONE) {
				return;
			}
			this._segCheck = (this._timeToDownload() * .8).toFixed(3);
			this._videoSource.appendBuffer(new Uint8Array(xhr.response));
			//setInterval(this._fileChecks.bind(this), 2000)
		}, false);
	}

	_fileChecks() {
		console.log('onTimeUpdate');
		if (!this._bufferUpdated) {
			return;
		}
		if ((this._videoElement.currentTime - this._lastTime) >= this._segCheck) {
			this._playSegment();
			this._lastTime = this._videoElement.currentTime;
			this._index++;
		}
		else {
			this._videoElement.removeEventListener('timeupdate', this.__fileChecks, false);
		}
	}

	_timeToDownload() {
		return (this.segmentDuration * 8 / this.bandwidth)
	}

	get currentSegment() {
		return this._representations
			.item(0).children.item(0).getAttribute('media').replace('$Number$', this._index);
	}

	get segmentDuration() {
		return this._representations.item(0).children.item(0).getAttribute('duration');
	}

	get mimeType() {
		return this._adaptationSet.getAttribute('mimeType') + '; codecs="' +
			this._adaptationSet.getAttribute('codecs') + '"';
	}

	get bandwidth() {
		return this._representations.item(0).getAttribute('bandwidth');
	}

	get initializationSegment() {
		return this._representations.item(0).children.item(0).getAttribute('initialization');
	}

	get startNumber() {
		return this._representations.item(0).children.item(0).getAttribute('startNumber');
	}
}