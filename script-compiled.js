'use strict';

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

var LivePlayer = (function () {
	function LivePlayer(url, videoId) {
		_classCallCheck(this, LivePlayer);

		this._index = 0;
		this._lastTime = 0;
		this._bufferUpdated = false;
		this._videoSource = null;
		this._storeBaseUrl(url);
		this._getData(url);
		this._videoElement = document.getElementById(videoId);
	}

	_createClass(LivePlayer, [{
		key: '_storeBaseUrl',
		value: function _storeBaseUrl(url) {
			this._baseUrl = url.split('/');
			this._baseUrl.pop();
			this._baseUrl = this._baseUrl.join('/') + '/';
		}
	}, {
		key: '_getData',
		value: function _getData(url) {
			var _this = this;

			if (!url) {
				return;
			}
			var xhr = new XMLHttpRequest();
			xhr.open('GET', url, true);
			xhr.responseType = 'text';
			xhr.send();

			xhr.onreadystatechange = function () {
				if (xhr.readyState === xhr.DONE) {
					var parser = new DOMParser();
					_this._xmlData = parser.parseFromString(xhr.response, 'text/xml', 0);
					_this._parseData();
				}
			};
		}
	}, {
		key: '_parseData',
		value: function _parseData() {
			this._adaptationSet = this._xmlData.querySelectorAll('AdaptationSet[mimeType*="video/"]').item(0);
			this._representations = this._adaptationSet.children;
			this._setupVideo();
		}
	}, {
		key: '_setupVideo',
		value: function _setupVideo() {
			var _this2 = this;

			if (!('MediaSource' in window)) {
				throw new Error('MediaSource not supported!');
			}
			this._mediaSource = new MediaSource();
			var url = URL.createObjectURL(this._mediaSource);
			this._videoElement.pause();
			this._videoElement.src = url;

			this._mediaSource.addEventListener('sourceopen', function () {
				_this2._videoSource = _this2._mediaSource.addSourceBuffer(_this2.mimeType);
				_this2._initVideo();
			}, false);
		}
	}, {
		key: '_initVideo',
		value: function _initVideo() {
			var _this3 = this;

			var xhr = new XMLHttpRequest();
			xhr.open('GET', this._baseUrl + this.initializationSegment);
			xhr.send();
			xhr.responseType = 'arraybuffer';
			xhr.addEventListener('readystatechange', function () {
				if (xhr.readyState !== xhr.DONE) {
					return;
				}
				_this3._videoSource.appendBuffer(new Uint8Array(xhr.response));
				_this3.__onSourceUpdate = _this3._onSourceUpdate.bind(_this3);
				_this3._videoSource.addEventListener('update', _this3.__onSourceUpdate, false);
			}, false);
		}
	}, {
		key: '_onSourceUpdate',
		value: function _onSourceUpdate() {
			this._bufferUpdated = true;
			this._getStarted();
			this._videoSource.removeEventListener('update', this.__onSourceUpdate, false);
			delete this.__onSourceUpdate;
		}
	}, {
		key: '_getStarted',
		value: function _getStarted() {
			this.__fileChecks = this._fileChecks.bind(this);
			this._videoElement.addEventListener("timeupdate", this.__fileChecks, false);
			this._index = this.startNumber;
			this._playSegment();
			this._index++;
		}
	}, {
		key: '_playSegment',
		value: function _playSegment() {
			var _this4 = this;

			var xhr = new XMLHttpRequest();
			xhr.open('GET', this._baseUrl + this.currentSegment);
			xhr.send();
			xhr.responseType = 'arraybuffer';
			xhr.addEventListener('readystatechange', function () {
				if (xhr.readyState !== xhr.DONE) {
					return;
				}
				_this4._segCheck = (_this4._timeToDownload() * .8).toFixed(3);
				_this4._videoSource.appendBuffer(new Uint8Array(xhr.response));
				//setInterval(this._fileChecks.bind(this), 2000)
			}, false);
		}
	}, {
		key: '_fileChecks',
		value: function _fileChecks() {
			console.log('onTimeUpdate');
			if (!this._bufferUpdated) {
				return;
			}
			if (this._videoElement.currentTime - this._lastTime >= this._segCheck) {
				this._playSegment();
				this._lastTime = this._videoElement.currentTime;
				this._index++;
			} else {
				this._videoElement.removeEventListener('timeupdate', this.__fileChecks, false);
			}
		}
	}, {
		key: '_timeToDownload',
		value: function _timeToDownload() {
			return this.segmentDuration * 8 / this.bandwidth;
		}
	}, {
		key: 'currentSegment',
		get: function get() {
			return this._representations.item(0).children.item(0).getAttribute('media').replace('$Number$', this._index);
		}
	}, {
		key: 'segmentDuration',
		get: function get() {
			return this._representations.item(0).children.item(0).getAttribute('duration');
		}
	}, {
		key: 'mimeType',
		get: function get() {
			return this._adaptationSet.getAttribute('mimeType') + '; codecs="' + this._adaptationSet.getAttribute('codecs') + '"';
		}
	}, {
		key: 'bandwidth',
		get: function get() {
			return this._representations.item(0).getAttribute('bandwidth');
		}
	}, {
		key: 'initializationSegment',
		get: function get() {
			return this._representations.item(0).children.item(0).getAttribute('initialization');
		}
	}, {
		key: 'startNumber',
		get: function get() {
			return this._representations.item(0).children.item(0).getAttribute('startNumber');
		}
	}]);

	return LivePlayer;
})();

//# sourceMappingURL=script-compiled.js.map