class HLSPlayer {
	constructor(videoId) {
		this._videoElement = document.getElementById(videoId);
		this.mimeType = 'video/mp4;codecs=avc1.42c00d,mp4a.40.2';
		this.segments = ['media_w249832652_10713.ts', 'media_w249832652_10714.ts', 'media_w249832652_10715.ts'];
		this._currentSegment = 0;
		this._setupVideo();
	}

	_setupVideo() {
		if (!('MediaSource' in window)) {
			throw new Error('MediaSource not supported!');
		}
		this._mediaSource = new window.MediaSource();
		var url = URL.createObjectURL(this._mediaSource);
		this._videoElement.pause();
		this._videoElement.src = url;

		this._mediaSource.addEventListener('sourceopen', () => {
			this._videoSource = this._mediaSource.addSourceBuffer(this.mimeType);
			this._videoSource.addEventListener('updateend', () => {
				if (this._videoElement.paused) {
					console.log('play');
					this._videoElement.play();
				}
			});
			this._initVideo();
		}, false);
	}

	_initVideo() {
		this.__onSourceUpdate = this._onSourceUpdate.bind(this);
		this._playSegment(this.segments[this._currentSegment]);
		this._videoSource.addEventListener('update', this.__onSourceUpdate, false);
	}

	_onSourceUpdate() {
		this._currentSegment++;
		this._playSegment(this.segments[this._currentSegment]);
		if (this._currentSegment >= this.segments.length - 1) {
			this._videoSource.removeEventListener('update', this.__onSourceUpdate, false);
		}
	}

	_playSegment(fileName) {
		var xhr = new XMLHttpRequest();
		xhr.open('GET', fileName);
		xhr.send();
		xhr.responseType = 'arraybuffer';
		xhr.addEventListener('readystatechange', (e) => {
			if (xhr.readyState !== xhr.DONE) {
				return;
			}
			var reader = new FileReader();
			reader.onload = (e) => {
				this._videoSource.appendBuffer(new Uint8Array(e.target.result));
			};
			reader.readAsArrayBuffer(new Blob([new Uint8Array(xhr.response)], {type: 'video/mp2t'}));
			/*try {
				this._videoSource.appendBuffer(new Uint8Array(xhr.response));
			} catch(exception){
				console.log(fileName, exception.message, exception.stack);
			}*/
		}, false);
	}
}